import json
import socket

def main(config):
    """
    Main application function
    :param:   config ConfigParser.ConfigParser
    :return:  None
    """
    data = {}
    modules = config.items('modules')
    for mod_set in modules:
        module = load_module(mod_set[0], config)
        data.update(module.get_data())
    packaged = package_data(data)
    transmit(packaged, config)


def load_module(module_name, config):
    module = __import__('mods.' + module_name, fromlist=[module_name])
    kwargs = {}
    if config.has_section(module_name):
        kwargs = {v[0]: v[1] for v in config.items(module_name)}
    return module.Module(**kwargs)

def package_data(data):
    return json.dumps(data)

def transmit(packaged, config):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(30)
    address = config.get('server', 'address')
    port = config.get('server', 'port')
    sock.sendto(packaged, (address, int(port)))
    sock.close()
