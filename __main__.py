from app.main import main
import argparse
import ConfigParser
import os

if __name__ == "__main__":
    '''entry point into sender'''
    config = ConfigParser.ConfigParser(allow_no_value=True)
    config.readfp(open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'sender.cfg')))
    app_func = main
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', dest='config', help="Path to config file")
    args = parser.parse_args()
    if args.config:
        config.read([args.config])
    main(config)

