class Module:

    def __init__(self, **kwargs):
        self.kwargs = kwargs
        self.data = {}
        self.init_data()

    def get_data(self):
        return self.data

    def init_data(self):
        """
        Override in modules to create data
        :return: None
        """

    def __str__(self):
        return str(self.data)
