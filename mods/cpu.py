from .abstract import Module as Parent
import psutil

class Module(Parent):

    def init_data(self):
        """
        handle cpu info
        :return:
        """
        self.data['cpu'] = {
            'perc_used': psutil.cpu_percent(),
        }
