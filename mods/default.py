from .abstract import Module as Parent
import socket

class Module(Parent):
    """
    Default information to send
    host name
    """

    def init_data(self):
        if 'host' in self.kwargs:
            self.data['host'] = self.kwargs['host']
        else:
            self.data['host'] = socket.gethostname()