from __future__ import absolute_import
from .abstract import Module as Parent
from docker import Client


class Module(Parent):

    def init_data(self):
        """
        handle docker containers
        :return:
        """
        self.data = {'docker': {}}
        client = Client(**self.kwargs)
        self.get_containers(client)

    def get_containers(self, client):
        containers = client.containers()
        for c in containers:
            detail = client.inspect_container(c.get('Id'))
            if bool(detail["State"]["Running"]):
                self.data['docker'][c.get('Id')] = c
                if 'version' not in self.kwargs or float(self.kwargs['version']) > 1.16:
                    stats = client.stats(c.get('Id'), decode=True)
                    stat = None
                    for stat in stats:
                        break
                    self.data['docker'][c.get('Id')]['cpu'] = self.get_cpu(stat)
                    self.data['docker'][c.get('Id')]['memory'] = self.get_memory(stat)
                self.data['docker'][c.get('Id')]['IP'] = detail["NetworkSettings"]["IPAddress"]

    def get_cpu(self, stat):
        cpu_stats = {}
        if stat:
            cpu_stats['user_mode'] = self.compute_percentage(stat['cpu_stats']['cpu_usage']['usage_in_usermode'], stat['cpu_stats']['system_cpu_usage'])
            cpu_stats['kernel_mode'] = self.compute_percentage(stat['cpu_stats']['cpu_usage']['usage_in_kernelmode'], stat['cpu_stats']['system_cpu_usage'])
        return cpu_stats

    def compute_percentage(self, use, total):
        if total:
            return float("{0:.2f}".format(float(use)/float(total) * 100))
        return 0

    def get_memory(self, stat):
        memory = {}
        if stat:
            memory['perc_used'] = self.compute_percentage(stat['memory_stats']['usage'], stat['memory_stats']['limit'])
        return memory
