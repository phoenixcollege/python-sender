from .abstract import Module as Parent
import psutil

class Module(Parent):

    def init_data(self):
        """
        handle processes
        :return:
        """
        self.data['processes'] = {}
        name = self.kwargs['name'] if 'name' in self.kwargs else None
        for proc in psutil.process_iter():
            if name is None:
                self.add_to_data(proc)
            else:
                if name in proc.name():
                    self.add_to_data(proc)

    def add_to_data(self, proc):
        pid = proc.pid
        name = proc.name()
        exe = proc.exe()
        cmdline = " ".join(proc.cmdline())
        cpu = proc.cpu_percent()
        mem = proc.memory_percent()
        self.data['processes'][pid] = {
            'name': name,
            'exe': exe,
            'cmdline': cmdline,
            'cpu_usage': cpu,
            'mem_usage': "{0:.2f}".format(mem),
        }