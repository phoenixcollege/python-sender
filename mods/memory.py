from .abstract import Module as Parent
import psutil

class Module(Parent):

    def init_data(self):
        """
        handle memory containers
        :return:
        """
        vmem = psutil.virtual_memory()
        self.data['memory'] = {
            'free': vmem.free,
            'available': vmem.available,
            'total': vmem.total,
            'perc_used': vmem.percent
        }